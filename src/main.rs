#[macro_use]
extern crate rocket;

use std::ffi::OsString;

fn get_hostname() -> String {
    let host = hostname::get().unwrap_or(OsString::from("no_hostname"));
    String::from(host.to_str().unwrap_or("no_hostname"))
}

#[get("/")]
fn index() -> String {
    format!(
        "Serving request: /
Hello, world!
Version: {}
Hostname: {}",
        option_env!("CARGO_PKG_VERSION").unwrap_or("unknown"),
        get_hostname()
    )
}

#[launch]
fn rocket() -> _ {
    rocket::build().mount("/", routes![index])
}

#[cfg(test)]
mod test {
    use super::rocket;
    use rocket::http::Status;
    use rocket::local::blocking::Client;

    #[test]
    fn hello_world() {
        let client = Client::tracked(rocket()).expect("valid rocket instance");
        let response = client.get("/").dispatch();

        let expected = format!(
            "Serving request: /
Hello, world!
Version: {}
Hostname: {}",
            option_env!("CARGO_PKG_VERSION").unwrap_or("unknown"),
            super::get_hostname()
        );

        assert_eq!(response.status(), Status::Ok);
        assert_eq!(response.into_string().unwrap(), expected);
    }
}
