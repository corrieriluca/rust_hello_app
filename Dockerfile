FROM rust:1.52.1 as builder
WORKDIR /usr/src/rust_hello_app
COPY . .
RUN cargo install --path .

FROM debian:buster-20210511-slim
COPY --from=builder /usr/local/cargo/bin/rust_hello_app /usr/local/bin/rust_hello_app

EXPOSE 8000

ENV ROCKET_CLI_COLORS=false
ENV ROCKET_ADDRESS=0.0.0.0

CMD ["rust_hello_app"]
